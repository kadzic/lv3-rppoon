using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp21
{
    class Program
    {

        static void Run(double[][] matrix)
        {
            for(int i = 0; i < matrix.Length; i++)
            {
                for(int j = 0; j < matrix[i].Length; j++)
                {
                    Console.Write(string.Format("{0}\t", matrix[i][j].ToString()));
                }
                Console.Write(Environment.NewLine + Environment.NewLine);
            }
        }

        static void Main(string[] args)
        {
            Singleton matrix = Singleton.GetInstance();

            Console.WriteLine("Enter number of rows and columns: ");

            int rows = Convert.ToInt32(Console.ReadLine());
            int cols = Convert.ToInt32(Console.ReadLine());

            Run(matrix.Matrix(rows, cols));

        }
    }
}
