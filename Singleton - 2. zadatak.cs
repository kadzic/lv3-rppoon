using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp21
{
    class Singleton
    {
        static Singleton instance;
        private Random random;

        private Singleton()
        {
            this.random = new Random();
        }

        public static Singleton GetInstance()
        {
            if(instance == null)
            {
                instance = new Singleton();
            }
            return instance;
        }

        public double[][] Matrix(int rows, int cols)
        {
            double[][] matrix = new double[rows][];
            for(int i = 0; i < rows; i++)
            {
                matrix[i] = new double[cols];
                for(int j = 0; j < cols; j++)
                {
                    matrix[i][j] = random.NextDouble();
                }
            }
            return matrix;
        }
    }
}
