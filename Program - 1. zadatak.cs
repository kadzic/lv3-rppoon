using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp21
{
    class Program
    {

        static void Run(IList<List<string>> data)
        {
            foreach(var i in data)
            {
                foreach(var j in i)
                {
                    Console.WriteLine(j);
                }
            }
        }

        static void Main(string[] args)
        {
            Dataset dataset = new Dataset("C:\\Users\\Korisnik\\source\\repos\\ConsoleApp21\\ConsoleApp21\\1. zadatak.txt");
            Run(dataset.GetData());
            Dataset prototype = dataset.Clone() as Dataset;
            dataset.ClearData();
            Run(prototype.GetData());

        }
    }
}